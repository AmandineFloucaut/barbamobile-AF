package com.barbajavas.barbamobile.repository;

import com.barbajavas.barbamobile.services.BarbiculeRepository;
import com.barbajavas.barbamobile.model.Barbabus;
import com.barbajavas.barbamobile.model.Barbamoto;
import com.barbajavas.barbamobile.model.Barbature;
import com.barbajavas.barbamobile.model.enumdata.Availability;
import org.junit.*;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;

import static org.junit.Assert.assertEquals;

public class BarbiculeRepositoryTest extends BarbiculeRepository {

    private Barbature barbatureTest;
    private Barbamoto barbamotoTest;
    private Barbabus barbabusTest;

    @BeforeAll
    public static void initAll(){
        System.out.println("Lancement des tests pour BarbiculeRepository");
    }

    @Before
    public void init(){
        System.out.println("Lancement d'un test");
        barbatureTest = new Barbature(
                "AAA-568-KK",
                Availability.FOR_RENT
        );
        barbamotoTest = new Barbamoto(
                "BBB-568-KK",
                Availability.FOR_RENT
        );
        barbabusTest = new Barbabus(
                "CCC-568-KK",
                Availability.FOR_RENT
        );
    }

    @Test
    public void saveBarbatureInRepo() {
        this.save(this.barbatureTest);
        this.save(this.barbamotoTest);
        this.save(this.barbabusTest);
        assertEquals(3, this.getBarbiculesList().size());
    }

    @Test 
    public void deleteBarbicle() {
        this.save(this.barbatureTest);
        this.save(this.barbamotoTest);
        this.save(this.barbabusTest);
        this.delete(this.barbabusTest.getMatriculation());
        assertEquals(2, this.getBarbiculesList().size());
    }

//    @Test
//    public void updateState(){
//        this.save(this.barbatureTest);
//        this.save(this.barbamotoTest);
//        this.save(this.barbabusTest);
//    }

    @After
    public void tearDown(){
        System.out.println("Fin du test");
    }

    @AfterAll
    public void tearDownAll(){
        System.out.println("Fin des tests");
    }
}
