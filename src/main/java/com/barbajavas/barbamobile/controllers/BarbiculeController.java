package com.barbajavas.barbamobile.controllers;

import com.barbajavas.barbamobile.model.Barbabus;
import com.barbajavas.barbamobile.model.Barbamoto;
import com.barbajavas.barbamobile.model.Barbature;
import com.barbajavas.barbamobile.model.Barbicule;
import com.barbajavas.barbamobile.model.enumdata.Availability;
import com.barbajavas.barbamobile.services.BarbiculeRepository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class BarbiculeController {

    BarbiculeRepository barbiculeRepo = new BarbiculeRepository();

    public static String sayHello(){
        return "Hello Barbajavas !";
    }

    public BarbiculeRepository displayAllBarbicule(){

        saveFakeData();
        return this.barbiculeRepo;

    }

    public void saveFakeData(){
        this.barbiculeRepo.save(new Barbabus("OHP-565-LL", Availability.FOR_RENT));
        this.barbiculeRepo.save(new Barbamoto("JJJ-562MM", Availability.AVAILABLE));
        this.barbiculeRepo.save(new Barbature("AAA-562MM", Availability.IN_REPAIR));
        this.barbiculeRepo.save(new Barbamoto("DDD-562MM", Availability.AVAILABLE));
    }
}
