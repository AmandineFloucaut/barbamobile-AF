package com.barbajavas.barbamobile.services;

import com.barbajavas.barbamobile.model.Barbabus;
import com.barbajavas.barbamobile.model.Barbamoto;
import com.barbajavas.barbamobile.model.Barbature;
import com.barbajavas.barbamobile.model.Barbicule;
import com.barbajavas.barbamobile.model.enumdata.Availability;
import com.barbajavas.barbamobile.model.enumdata.Barbanoise;

import java.util.*;
import java.util.stream.Stream;

public class BarbiculeRepository {
    private HashMap<String, Barbicule> barbiculesList;

    public BarbiculeRepository(){
        this.barbiculesList = new HashMap<String, Barbicule>();
    }

    public void save(Barbicule barbicule){
        this.barbiculesList.put(barbicule.getMatriculation(), barbicule);
    }

    public void delete(String matriculation){
        this.barbiculesList.remove(matriculation);
    }

    public Optional<Barbicule> getOne(String matriculation){
        return Optional.ofNullable(barbiculesList.get(matriculation));
    }

    public void updateState(String matriculation, String newState){
        Barbicule currentBarbicule = this.getOne(matriculation)
                                         .orElseThrow(() -> new IllegalArgumentException(
                                                 "Ce barbicule n'existe pas !"
                                         ));
        Availability state = this.manageState(newState);
        currentBarbicule.setState(state);
    }

    public void updateMatriculation(String matriculation, String newMatriculation){
        Barbicule currentBarbicule = this.getOne(matriculation)
                                         .orElseThrow(() -> new IllegalArgumentException(
                                                 "Ce barbicule n'existe pas !"
                                         ));
        currentBarbicule.setMatriculation(newMatriculation);
    }

    public Stream<Barbicule> barbiculesByState(String availability){
        ArrayList<Barbicule> barbiculesbyState = new ArrayList<>();

        for(Map.Entry<String, Barbicule> entry: this.barbiculesList.entrySet()){

            if(entry.getValue().getState().equals(this.manageState(availability))){
                barbiculesbyState.add(entry.getValue());
            }
        }
        return barbiculesbyState.stream();
    }

    public Optional<Barbanoise> getNoiseCurrentBarbicule(String matriculation){
        Barbicule currentBarbicule = this.getOne(matriculation)
                .orElseThrow(() -> new IllegalArgumentException("Ce barbicule n'existe pas !"));
        return this.getOne(matriculation).map(barbicule -> currentBarbicule.getNoise());
    }

    public int numberBarbiculeByType(String currentType){
        List<Barbicule> barbiculesByType = new ArrayList<>();

        for(Map.Entry<String, Barbicule> entry: this.barbiculesList.entrySet()){
            barbiculesByType.add((Barbicule) entry);
        }

        switch (currentType){
            case "barbature" -> barbiculesByType = barbiculesByType.stream()
                                                .filter(barbicule -> (
                                                        barbicule instanceof Barbature
                                                )).toList();
            case "barbamoto" -> barbiculesByType = barbiculesByType.stream()
                                                .filter(barbicule -> (
                                                        barbicule instanceof Barbamoto
                                                )).toList();
            case "barbabus" -> barbiculesByType = barbiculesByType.stream()
                                               .filter(barbicule -> (
                                                       barbicule instanceof Barbabus
                                               )).toList();
        }

//        if(currentType.equalsIgnoreCase("barbature")){
//            barbiculesByType.stream().filter(barbicule -> (barbicule instanceof Barbature));
//        } else if(currentType.equalsIgnoreCase("barbamoto")){
//            barbiculesByType.stream().filter(barbicule -> (barbicule instanceof Barbamoto));
//        }  else if(currentType.equalsIgnoreCase("barbabus")){
//            barbiculesByType.stream().filter(barbicule -> (barbicule instanceof Barbabus));
//        }
        return barbiculesByType.size();
    }

    public Availability manageState(String currentState){
        Availability state;

        if(currentState.toUpperCase() == "available"){
            state = Availability.AVAILABLE;
        } else if(currentState.toUpperCase() == "for_rent"){
            state = Availability.FOR_RENT;
        } else if(currentState.toUpperCase() == "in_repair"){
            state = Availability.IN_REPAIR;
        } else {
            throw new RuntimeException(
                    "Vous devez indiquer un nouvel état pour le barbicule !"
            );
        }

        return state;
    }

    public List getBarbiculesList() {
        List listBarb = new ArrayList<>();
        for(Map.Entry<String, Barbicule> entry: barbiculesList.entrySet()){
            listBarb.add(entry.getValue());
        }
        return listBarb;
    }

    @Override
    public String toString() {
        String listBarb = "";
        int index = 1;
        for(Map.Entry<String, Barbicule> entry: barbiculesList.entrySet()){
            listBarb += entry.getClass().getName() + " n° " + index + " : \n";
            listBarb += entry.getValue().getMatriculation() + ", ";
            listBarb += entry.getValue().getState() + ", ";
            listBarb += entry.getValue().getFuelType() + ", ";
            listBarb += entry.getValue().getNumberWheels()+ ", ";
            listBarb += entry.getValue().getNoise() + ".\n";
            index++;
        }
        return listBarb;
    }

    public void setBarbiculesList(HashMap<String, Barbicule> barbiculesList) {
        this.barbiculesList = barbiculesList;
    }

}
