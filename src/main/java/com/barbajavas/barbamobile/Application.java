package com.barbajavas.barbamobile;

import com.barbajavas.barbamobile.controllers.BarbiculeController;

public class Application {

    public static void main(String[] args){

        BarbiculeController.sayHello();

        BarbiculeController barbiculeContr = new BarbiculeController();
        System.out.println(barbiculeContr.displayAllBarbicule());
    }
}
