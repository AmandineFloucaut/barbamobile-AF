package com.barbajavas.barbamobile.model;

import com.barbajavas.barbamobile.model.enumdata.Availability;
import com.barbajavas.barbamobile.model.enumdata.Barbafuel;
import com.barbajavas.barbamobile.model.enumdata.Barbanoise;

public class Barbature extends Barbicule {

    public Barbature(String matriculation,Availability state){
        super(3, Barbafuel.BARBASIEL, Barbanoise.BARBAVROUM, matriculation, state);
    }
}
