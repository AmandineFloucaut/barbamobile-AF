package com.barbajavas.barbamobile.model;

import com.barbajavas.barbamobile.model.enumdata.Availability;
import com.barbajavas.barbamobile.model.enumdata.Barbafuel;
import com.barbajavas.barbamobile.model.enumdata.Barbanoise;

public class Barbabus extends Barbicule {

    public Barbabus(String matriculation,Availability state) {
        super(4, Barbafuel.BARBALIGHT, Barbanoise.BARBAPOUET, matriculation, state);
        this.matriculation = matriculation;
        this.state = state;
    }
}
