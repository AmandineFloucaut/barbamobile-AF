package com.barbajavas.barbamobile.model;

import com.barbajavas.barbamobile.model.enumdata.Availability;
import com.barbajavas.barbamobile.model.enumdata.Barbafuel;
import com.barbajavas.barbamobile.model.enumdata.Barbanoise;

public class Barbicule {
    protected final int numberWheels;
    protected final Barbafuel fuelType;
    protected final Barbanoise noise;
    protected String matriculation;
    protected Availability state;

    public Barbicule(int numberWheels, Barbafuel fuelType, Barbanoise noise, String matriculation, Availability state){
        this.numberWheels = numberWheels;
        this.fuelType = fuelType;
        this.noise = noise;
        this.matriculation = matriculation;
        this.state = state;
    }

    public int getNumberWheels() {
        return numberWheels;
    }

    public Barbafuel getFuelType() {
        return fuelType;
    }

    public Barbanoise getNoise() {
        return noise;
    }

    public String getMatriculation() {
        return matriculation;
    }

    public void setMatriculation(String matriculation) {
        this.matriculation = matriculation;
    }

    public Availability getState() {
        return state;
    }

    public void setState(Availability state) {
        this.state = state;
    }


}
