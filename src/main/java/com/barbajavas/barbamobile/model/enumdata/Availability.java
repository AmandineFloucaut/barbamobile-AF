package com.barbajavas.barbamobile.model.enumdata;

public enum Availability {
    AVAILABLE,
    FOR_RENT,
    IN_REPAIR,
}
