package com.barbajavas.barbamobile.model;

import com.barbajavas.barbamobile.model.enumdata.Availability;
import com.barbajavas.barbamobile.model.enumdata.Barbafuel;
import com.barbajavas.barbamobile.model.enumdata.Barbanoise;

public class Barbamoto extends Barbicule {
    public Barbamoto(String matriculation,Availability state) {
        super(2, Barbafuel.BARBAFUEL, Barbanoise.BARBADAM, matriculation, state);
        this.matriculation = matriculation;
        this.state = state;
    }
}
