package com.barbajavas.barbamobile.factory;

import com.barbajavas.barbamobile.model.Barbamoto;
import com.barbajavas.barbamobile.model.Barbicule;
import com.barbajavas.barbamobile.model.enumdata.Availability;

public class BarbiculeFactory {

    public static Barbicule newBarbature(){
        Barbicule b = new Barbamoto("AAA-123-BC", Availability.IN_REPAIR);
        return b;
    }
}
