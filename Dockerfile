# Pull first image for maven package
FROM maven:3.8.1-openjdk-17 AS builder
# créer un dossier app dans le container
WORKDIR /app
# copy le pom.xml dedans
COPY pom.xml .
# lance le package de l'app avec maven
RUN mvn clean -e -B package

# Pull 2nd image for create and build fileApp.jar
FROM openjdk:slim
# Récupère le dossier /app
WORKDIR /app
# Copy le fichier .jar dedans (en éxécutant la 1ère image, grâce au mot clé builder)
COPY --from=builder /app/target/Barbamobile-1.0-SNAPSHOT.jar .
# Indique la commande java et le fichier à éxécuter
ENTRYPOINT ["java", "-jar", "./Barbamobile-1.0-SNAPSHOT.jar"]